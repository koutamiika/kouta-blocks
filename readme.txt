=== Kouta Blocks ===
Contributors: Miika Salo
Tags: acf, acf blocks, gutenberg, block, gutenberg blocks
Requires at least: 4.7
Requires PHP: 5.6
Tested up to: 5.2
Stable tag: 0.6.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Custom blocks for Gutengerg Block Editor

== Description ==

<ul>
<li>Sosiaalisen median jakopainikkeet</li>
<li>Artikkelit</li>
<li>Testimoniaali</li>
<li>Juokseva Numero</li>
<li>Kuvakaruselli</li>
</ul>

<strong>Tärkeää:</strong> Vaatii Advanced Custom Fields 5.8.

== Installation ==

1. Install Kouta Blocks by uploading the files to your server at wp-content/plugins.

2. Activate the plugin and done. You should be able to see new Kouta Blocks in your Gutenberg editor.

== Changelog ==

= 0.6.4 =
* Fixed slider block issue.

= 0.6 =
* Added update checker

= 0.5 =
* Social Sharing Block

= 0.4 =
* Testimonial Block

= 0.3 =
* Counter Numbers block

= 0.2 =
* Slider Block

= 0.1 =
* Posts Block