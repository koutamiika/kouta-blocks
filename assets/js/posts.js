(function($){

    /**
     * initializeBlock
     *
     * Adds custom JavaScript to the block HTML.
     *
     * @date    15/4/19
     * @since   1.0.0
     *
     * @param   object $block The block jQuery element.
     * @param   object attributes The block attributes (only available when editing).
     * @return  void
     */
    var initializeBlock = function( $block ) {
        $(function() {
            var Accordion = function(el, multiple) {
                    this.el = el || {};
                    this.multiple = multiple || false;
    
                    var links = this.el.find('.article-title');
                    links.on('click', {
                            el: this.el,
                            multiple: this.multiple
                    }, this.dropdown)
            }
    
            Accordion.prototype.dropdown = function(e) {
                    var $el = e.data.el;
                    $this = $(this),
                            $next = $this.next();
    
                    $next.slideToggle();
                    $this.parent().toggleClass('open');
    
                    if (!e.data.multiple) {
                            $el.find('.accordion-content').not($next).slideUp().parent().removeClass('open');
                    };
            }
            var accordion = new Accordion($('.accordion-container'), false);
    });
    }
  
    // Initialize each block on page load (front end).
    $(document).ready(function(){
        $('.kb-blocks-posts-block').each(function(){
            initializeBlock( $(this) );
        });
    });
  
    // Initialize dynamic block preview (editor).
    if( window.acf ) {
        window.acf.addAction( 'render_block_preview/type=block-kouta-blocks-posts', initializeBlock );
    }
  
  })(jQuery);