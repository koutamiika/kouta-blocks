
<?php 
    $kb_thumb = '';
    if ( has_post_thumbnail() ){
        $kb_thumb = 'has-thumbnail';
    } else{
        $kb_thumb = 'no-thumbnail';
    }
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $kb_thumb ); ?>>

    <h4 class="article-title"><i></i><?php the_title(); ?>  <span class="kb-post-date">- <?php echo get_the_time( get_option( 'date_format' ) ); ?></span> </h4>
    <div class="accordion-content">
        <?php the_content(); ?>
    </div>

</article>