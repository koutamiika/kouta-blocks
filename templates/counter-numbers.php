<?php 

    $id = $block['id'];

    $className = 'kb-counter-numbers';

    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }

    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

    $bg = get_field('kb_counter_number_bg');

?>

<div class="<?php echo $id; ?> <?php echo esc_attr($className); ?>" <?php echo ( $bg ? 'style="background-image:url('.$bg['url'].')"' : ''); ?>>

    <div class="kb-counter-number-inner">

        <div class="kb-counter-number-title"><?php the_field('kb_counter_number_title'); ?></div>
        <div class="kb-counter-numbers-wrapper">
            <span class="kb-counter-number-prefix"><?php the_field('kb_counter_number_prefix'); ?></span>
            <span class="kb-counter-number"><?php the_field('kb_counter_number'); ?></span>
            <span class="kb-counter-number-suffix"><?php the_field('kb_counter_number_suffix'); ?></span>
        </div>
        <div class="kb-counter-number-subtitle"><?php the_field('kb_counter_number_subtitle'); ?></div>

    </div>

</div>