<?php

$block_id    = $block['id'];
$block_class = 'kb-blocks-slider-block';

if ( ! empty( $block['className'] ) ) {
	$block_class .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$block_class .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_html( $block_id ); ?>" class="<?php echo esc_attr( $block_class ); ?>">

	<?php if ( have_rows( 'kb_slider_images' ) ) : ?>

		<?php
			/**
			 * Slick options needs to added as data-attributes because acf_register_block_type(), wp_localize_script() and get_field() don't play well together as of ACF 5.8.1
			 * wp_localize_script() works inside enqueue_assets but it doesn't fetch correct ID.
			 */
		?>

		<div 
			class="slides" 
			data-autoplay="<?php the_field( 'kb_slider_autoplay' ); ?>" 
			data-slides="<?php the_field( 'kb_slider_slides_to_show' ); ?>">

		<?php 
		while ( have_rows( 'kb_slider_images' ) ) :
			the_row();
		?>

			<?php 
				$img = get_sub_field( 'kb_slider_image' ); 
			?>

			<img src="<?php echo esc_url( $img['url'] ); ?>" alt="<?php echo esc_html( $img['alt'] ); ?>" title="<?php echo esc_html( $img['title'] ); ?>">

		<?php endwhile; ?>

		</div>

	<?php endif; ?>

</div>
