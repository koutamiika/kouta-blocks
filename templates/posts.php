<?php 

    $id = $block['id'];
    $className = 'kb-blocks-posts-block';

    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

?>

<div class="<?php echo $id; ?> <?php echo esc_attr($className); ?>">

    <?php 
        $kb_posts_layout = get_field('kb_posts_layout');
        $kb_posts_per_page = get_field('kb_posts_per_page');
        $kb_posts_columns = get_field('kb_posts_columns');
    ?>

    <?php

    $kb_grid_col = '';
    if( $kb_posts_layout == 'grid' ){
 
        $kb_grid_col = "kb-posts-columns-" . $kb_posts_columns; 

    }

    $kb_cat = get_field( 'kb_posts_category' );
    $kb_cat_names = array();  

    if( is_array($kb_cat) ){

        foreach($kb_cat as $catskey => $catsval){

            $kb_cat_names[] = $catsval;

        }

    }

    /**
     * Get post types. Defaults to post.
     */
    $kb_posts_types = get_field('kb_posts_type') ?: 'post';

    $args = array(
        'post_type' => $kb_posts_types,
        'post_status' => 'publish',
        'posts_per_page' => $kb_posts_per_page, 
        'cat' => $kb_cat_names,
    );


    // the query
    $query = new WP_Query( $args ); ?>

    <?php if ( $query->have_posts() ) : ?>

    <div class="kb-blocks-post-<?php echo $kb_posts_layout; ?> <?php echo $kb_grid_col; ?>">

        <?php if ( $kb_posts_layout == 'accordion' ) : ?>

        <div id="accordion" class="accordion-container">

        <?php endif; ?>

        <?php while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php if ( $kb_posts_layout == 'list' || $kb_posts_layout == 'grid' ) : ?>

                <?php include('content-post.php'); ?>

            <?php endif; ?>

            <?php if ( $kb_posts_layout == 'accordion' ) : ?>

                <?php include('content-post-accordion.php'); ?>

            <?php endif; ?>

        <?php endwhile; ?>

        <?php if ( $kb_posts_layout == 'accordion' ) : ?>

            </div>

        <?php endif; ?>


    </div>

    <?php wp_reset_postdata(); ?>

    <?php else : ?>

        <p><?php esc_html_e( 'Yhtään artikkelia ei löytynyt.', 'kouta-blocks' ); ?></p>

    <?php endif; ?>

</div>