<?php 

    $id = $block['id'];

    $kb_permalink = get_permalink();
    $kb_pagetitle = get_the_title();

    $className = 'kb-social-sharing';

    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }

    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

    $links = get_field('kb_social_sharing_buttons');

    $kb_socialLinks = array( 
        'facebook' => "https://facebook.com/sharer/sharer.php?u={$kb_permalink}",
        'twitter' => "https://twitter.com/intent/tweet/?text={$kb_pagetitle}&amp;url={$kb_permalink}",
        'linkedin' => "https://www.linkedin.com/shareArticle?mini=true&amp;url={$kb_permalink}&amp;title={$kb_pagetitle}&amp;summary={$kb_pagetitle}&amp;source={$kb_permalink}",
        'pinterest' => "https://pinterest.com/pin/create/button/?url={$kb_permalink}&amp;media=http%3A%2F%2Fsharingbuttons.io&amp;description={$kb_pagetitle}.", 
        'whatsapp' => "whatsapp://send?text={$kb_pagetitle}.{$kb_permalink}", 
        'reddit' => "https://reddit.com/submit/?url={$kb_permalink}&amp;resubmit=true&amp;title={$kb_pagetitle}", 
        'xing' => "https://www.xing.com/app/user?op=share;url={$kb_permalink};title={$kb_pagetitle}", 
        'email' => "mailto:?subject={$kb_pagetitle}&amp;body={$kb_permalink}",
        'vk' => "http://vk.com/share.php?title={$kb_pagetitle}&amp;url={$kb_permalink}"
    );

?>

<div class="<?php echo $id; ?> <?php echo esc_attr($className); ?>">

    <ul class="kb-social-sharing-links">

        <?php foreach ( $links as $key => $value ) : ?>

            <li class="kb-social-sharing-<?php echo $value; ?>"><a href="<?php echo $kb_socialLinks[$value]; ?>"><?php echo ucfirst( $value ); ?></a></li>

        <?php endforeach; ?>

    </ul>

</div>