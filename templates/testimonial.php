<?php 

    $id = $block['id'];
    $className = 'kb-testimonial';

    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

    $txt = get_field('kb_testimonial_text') ?: 'Lorem ipsum dolor set amet...';
    $name = get_field('kb_testimonial_name') ?: 'John Doe';
    $title = get_field('kb_testimonial_title') ?: 'CEO';
    $avatar = get_field('kb_testimonial_avatar');
    
?>

<div class="<?php echo $id; ?> <?php echo esc_attr($className); ?>">

    <?php do_action('after_kb_testimonial_begin'); ?>

    <div class="kb-testimonial-wrapper">

        <?php if ( $txt ) : ?>
        <div class="kb-testimonial-quote">
            <blockquote>
                <?php esc_html_e( $txt ); ?>
            </blockquote>
        </div>
        <?php endif; ?>

        <div class="kb-testimonial-bio">

            <?php if ( $avatar ) : ?>
            <div class="kb-testimonial-avatar">

                <img src="<?php echo $avatar['url']; ?>" alt="<?php echo $avatar['alt']; ?>">

            </div>
            <?php endif; ?>

            <div class="kb-testimonial-bio-info">

                <h2><?php esc_html_e($name); ?></h2>

                <?php if ( $title ) : ?>
                <p><small><?php esc_html_e( $title ); ?></small></p>
                <?php endif; ?>

            </div>

        </div>

    </div>

    <?php do_action('before_kb_testimonial_end'); ?>

</div>