
<?php 
    $kb_thumb = '';
    if ( has_post_thumbnail() ){
        $kb_thumb = 'has-thumbnail';
    } else{
        $kb_thumb = 'no-thumbnail';
    }
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $kb_thumb ); ?>>

    <div class="kb-blocks-post-content">

    <?php if( have_rows('kb_posts_elements') ) : ?>

        <?php while ( have_rows('kb_posts_elements') ) : the_row(); ?>

            <?php if( get_row_layout() == 'kb_posts_element_thumbnail' ): ?>

                <div class="kb-blocks-post-thumbnail">	
                    <?php the_post_thumbnail(); ?>
                </div>

            <?php endif; ?>

            <?php if( get_row_layout() == 'kb_posts_element_title' ): ?>

                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <?php endif; ?>

            <?php if( get_row_layout() == 'kb_posts_element_meta' ): ?>

                <div class="kb-blocks-entry-meta">

                    <?php if ( apply_filters('kb_posts_post_author', true) ) : ?>
                        <span class="kb-post-author"><?php the_author(); ?></span>
                    <?php endif; ?>

                    <?php if ( apply_filters('kb_posts_post_date', true) ) : ?>
                        <span class="kb-post-date"><?php echo get_the_time( get_option( 'date_format' ) ); ?></span>
                    <?php endif; ?> 
                    
                </div>

            <?php endif; ?>
            
            <?php if( get_row_layout() == 'kb_posts_element_excerpt' ): ?>

                <div class="kb-blocks-entry-content">
                    <?php the_excerpt(); ?>
                </div>

            <?php endif; ?>

            <?php if( get_row_layout() == 'kb_posts_element_read_more' ): ?>

                <div class="kb-post-button">
                    <a href="<?php the_permalink(); ?>" class="kb_post_btn"><?php esc_html_e('Lue lisää', 'kouta-blocks'); ?></a>
                </div>

            <?php endif; ?>
            
        <?php endwhile; ?>

    <?php endif; ?>

    </div>

</article>