<?php
/**
 * Plugin Name: Kouta Blocks
 * Description: Kouta Gutenberg Blocks.
 * Version:     0.6.4
 * Author:      Kouta Media Oy
 * Author URI:  https://www.koutamedia.fi
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: kouta-blocks
 * Domain Path: /languages
 */

define( 'KOUTA_BLOCKS_DIR', plugin_dir_path( __FILE__ ) );
define( 'KOUTA_BLOCKS_TEMPLATE_DIR', KOUTA_BLOCKS_DIR . '/templates/' );

/**
 * Check plugin updates from BitBucket repo
 */
require 'plugin-update-checker/plugin-update-checker.php';
$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/koutamiika/kouta-blocks',
	__FILE__,
	'kouta-blocks'
);

/**
 * Check if ACF is installed.
 */
function kouta_blocks_acf_dependency() {

	if( ! class_exists( 'ACF' ) ) {
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die( __( 'Asenna ja aktivoi Advanced Custom Fields Pro.', 'kouta-blocks' ), 'Lisäosa vaadittu', array( 'back_link' => true ) );
	}

}

/**
 * Setup activation hook.
 */
register_activation_hook( __FILE__, 'kouta_blocks_acf_dependency' );

/**
 * Temple block templates. Check if template exists in theme, else use original plugin file.
 *
 * @return block file path
 */
function kouta_blocks_get_template( $file ) {
	$real_file = $file . '.php';

	// Look for a file in theme
	if ( $theme_template = locate_template('kouta-blocks/' . $real_file ) ) {

		return $theme_template;

	} else {

		// Nothing found, let's look in our plugin
		$plugin_template = KOUTA_BLOCKS_TEMPLATE_DIR . '/' . $real_file;

		if ( file_exists( $plugin_template ) ) {

			return $plugin_template;

		}

	}

}

/**
 * Register Blocks.
 */
function register_kouta_blocks() {

	/**
	 * Check if acf_register_block_type function exists
	 */
	if ( function_exists( 'acf_register_block_type' ) ) {

		/**
		 * Register Posts Block.
		 */
		acf_register_block_type( array(
			'name'            => 'kouta-blocks-posts',
			'mode'            => 'preview',
			'title'           => __( 'Artikkelit', 'kouta-blocks' ),
			'description'     => __( 'Näyttää artikkelit listana tai ruudukkona.', 'kouta-blocks' ),
			'render_template' => kouta_blocks_get_template( 'posts' ),
			'category'        => 'common',
			'icon'            => 'admin-post',
			'enqueue_assets'  => function() {
				wp_enqueue_style( 'kb-posts-css', plugin_dir_url( __FILE__ ) . 'assets/css/posts.css' );
				wp_enqueue_script( 'kb-posts-js', plugin_dir_url( __FILE__ ) . 'assets/js/posts.js', array(), '1.0', true  );
			},
		) );

		global $post;

		/**
		 * Register Slider Block.
		 */
		acf_register_block_type( array(
			'name'              => 'kouta-blocks-slider',
			'mode'              => 'preview',
			'title'             => __('Kuvakaruselli', 'kouta-blocks'),
			'description'       => __('Näyttää kuvakarusellin.', 'kouta-blocks'),
			'render_template'   => kouta_blocks_get_template('slider'),
			'category'          => 'common',
			'icon'              => 'images-alt',
			'enqueue_assets' => function(){
				wp_enqueue_style( 'kb-slider-css', plugin_dir_url( __FILE__ ) . 'assets/css/slider.css' );
				wp_enqueue_script( 'kb-slider-js', plugin_dir_url( __FILE__ ) . 'assets/js/slider.min.js', array(), '1.0', true );
			},
		) );

		/**
		 * Register Counter Numbers Block.
		 */
		acf_register_block_type( array(
			'name'              => 'kouta-blocks-counter-numbers',
			'mode'              => 'preview',
			'title'             => __('Juoksevat numerot', 'kouta-blocks'),
			'description'       => __('Näyttää juoksevan numeron.', 'kouta-blocks'),
			'render_template'   => kouta_blocks_get_template('counter-numbers'),
			'category'          => 'common',
			'icon'              => 'editor-textcolor',
			'enqueue_assets' => function(){
			  wp_enqueue_style( 'kb-counter-numbers-css', plugin_dir_url( __FILE__ ) . 'assets/css/counter-numbers.css' );
			  wp_enqueue_script( 'kb-counter-numbers-js', plugin_dir_url( __FILE__ ) . 'assets/js/counter-numbers.js', array(), '1.0', true );
			},
		) );

		/**
		 * Register Testimonial Block.
		 */
		acf_Register_block_type( array(
			'name'              => 'kouta-blocks-testimonial',
			'mode'              => 'preview',
			'title'             => __('Testimoniaali', 'kouta-blocks'),
			'description'       => __('Näyttää Testimoniaalin', 'kouta-blocks'),
			'render_template'   => kouta_blocks_get_template('testimonial'),
			'category'          => 'common',
			'icon'              => 'testimonial',
			'enqueue_assets' => function(){
			  wp_enqueue_style( 'kb-testimonial-css', plugin_dir_url( __FILE__ ) . 'assets/css/testimonial.css' );
			},
		) );

		/**
		 * Register Testimonial Block.
		 */
		acf_register_block_type( array(
			'name'              => 'kouta-blocks-social-share',
			'mode'              => 'preview',
			'title'             => __('Some jakopainikkeet', 'kouta-blocks'),
			'description'       => __('Näyttää sosiaalisen media jakopainikkeet', 'kouta-blocks'),
			'render_template'   => kouta_blocks_get_template('social-sharing'),
			'category'          => 'common',
			'icon'              => 'twitter',
			'enqueue_assets' => function(){
			  wp_enqueue_style( 'kb-social-sharing-css', plugin_dir_url( __FILE__ ) . 'assets/css/social-sharing.css' );
			},
		) );


	}

}
add_action('acf/init', 'register_kouta_blocks');


/**
 * Save ACF JSON
 */
add_filter('acf/settings/save_json', 'kouta_blocks_json_save_point');
function kouta_blocks_json_save_point( $plugin_path ) {

	// update path
	$plugin_path = plugin_dir_path( __FILE__ ) . '/acf-json';

	// return
	return $plugin_path;

}


/**
 * Load ACF JSON
 */
add_filter('acf/settings/load_json', 'kouta_blocks_json_load_point');
function kouta_blocks_json_load_point( $plugin_path ) {

	// remove original path (optional)
	unset($plugin_path[0]);

	// append path
	$plugin_path[] = plugin_dir_path( __FILE__ ) . '/acf-json';

	// return
	return $plugin_path;

}

/**
 * Default value for posts elements
 */
add_filter('acf/load_value/name=kb_posts_elements', 'kouta_blocks_posts_elements_defaults', 10, 3);
function  kouta_blocks_posts_elements_defaults($value, $post_id, $field) {

	if ($value !== NULL) {
	  // $value will only be NULL on a new post
	  return $value;
	}
	// add default layouts
	$value = array(
		array(
			'acf_fc_layout' => 'kb_posts_element_thumbnail'
		),
		array(
			'acf_fc_layout' => 'kb_posts_element_title'
		),
		array(
			'acf_fc_layout' => 'kb_posts_element_meta'
		),
		array(
			'acf_fc_layout' => 'kb_posts_element_excerpt'
		),
		array(
			'acf_fc_layout' => 'kb_posts_element_read_more'
		)
		);

	return $value;

}

/**
 * populate acf field (kb_posts_type) with post types
 */
function kouta_blocks_posts_type_defaults( $field ) {
	$field['choices'] = kouta_blocks_get_post_types();
	return $field;
}
add_filter( 'acf/load_field/name=kb_posts_type', 'kouta_blocks_posts_type_defaults' );

function kouta_blocks_get_post_types() {
	$values = array();

	$args = array(
		'public'   => true,
	 );

	 $post_types = get_post_types( $args );

	 foreach ( $post_types  as $post_type ) {

		$values[$post_type] = $post_type;

	 }

	return $values;
}