��          T      �       �      �       �   "   �   =     E   M  -   �  �  �     �  "   �  2   �  &   �  3        G                                        Check for updates There is no changelog available. Unknown update checker status "%s" the plugin titleA new version of the %s plugin is available. the plugin titleCould not determine if updates are available for %s. the plugin titleThe %s plugin is up to date. Project-Id-Version: plugin-update-checker
POT-Creation-Date: 2017-11-24 17:02+0200
PO-Revision-Date: 2019-06-04 09:51+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_x
Last-Translator: 
Language: fi
X-Poedit-SearchPath-0: .
 Tarkista päivitykset Muutoshistoriaa ei ole saatavilla. Päivitysten tarkistajan tuntematon tila “%s”. Lisäosaan %s on saatavilla päivitys. Lisäosan %s päivityksia ei pystytty tarkistamaan. %s lisäosa on ajantasalla. 